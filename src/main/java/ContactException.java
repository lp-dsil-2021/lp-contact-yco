

import java.io.IOException;

public class ContactException extends IOException {
	
	public ContactException() { super(); }
    public ContactException(String s) {super(s); }

}
