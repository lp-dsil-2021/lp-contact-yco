

import java.util.ArrayList;

public class ContactDAO implements IContactDao{
	
	private ArrayList<Contact> listContact;
	
	public ContactDAO() {
		listContact = new ArrayList<Contact>();
	}
	
	public void addContact(Contact newContact) {
		this.listContact.add(newContact);
	}
	
	public Contact getIndexContact(int index) {
		return this.listContact.get(index);
	}
	
	public Contact getLastContact() {
		return this.listContact.get((this.listContact.size() - 1));
	}
	
	private Contact recursSearchContact(String name, int index) {
		if (index >= listContact.size() - 1) 
			return null;
		else if (listContact.get(index).getName().equals(name)) 
			return listContact.get(index);
		return recursSearchContact(name, ++index);
	}

	public Contact searchContact(String name) {
		return recursSearchContact(name, 0);
	}
	
	public boolean removeContact(Contact contact) {
		return this.listContact.remove(contact);
	}
	
	public boolean removeContact(String name) {
		Contact foundContact = searchContact(name); 
		if (foundContact != null)
			return removeContact(foundContact);
		return false;
	}
	
	public boolean contains(Contact contact) {
		return this.listContact.stream().anyMatch(x -> x.getName().equalsIgnoreCase(contact.getName()));
	}
}
