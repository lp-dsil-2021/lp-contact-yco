
public interface IContactDao {
	void 	addContact(Contact contact);
	Contact	getIndexContact(int index);
	Contact getLastContact();
	Contact	searchContact(String name);
	boolean removeContact(Contact contact);
	boolean removeContact(String name);
	boolean contains(Contact contact);
}
