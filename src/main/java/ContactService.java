

public class ContactService implements IContactService {
	
	private IContactDao contactDAO;
	
	public ContactService() {
		contactDAO = new ContactDAO();
	}

	@Override
	public Contact creerContact(String nom) throws ContactException {
		Contact newContact = new Contact(nom);
		
		if (nom == null || nom.trim().length() < 3 || nom.length() > 40 || contactDAO.contains(newContact)) {
			throw new ContactException();
		}
		
		contactDAO.addContact(newContact);
		
		return newContact;
	}
	
	public boolean removeContact(Contact contact) {
		return contactDAO.removeContact(contact);
	}

	public boolean removeContact(String name) {
		return contactDAO.removeContact(name);
	}
	
	public Contact search(String nom) throws ContactException {
		Contact contact =  contactDAO.searchContact(nom);
		if(contact == null) {
			throw new ContactException();
		}
		return contact;
	}
}
