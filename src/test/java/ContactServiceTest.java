import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ContactServiceTest {

	private ContactService contactService;
	
	public ContactServiceTest() throws ContactException {
		// TODO Auto-generated constructor stub
		contactService = new ContactService();
	}
	
	@Test
	public void testCreerContactNull() {
		Assertions.assertThrows(ContactException.class, () -> this.contactService.creerContact(null));
	}
	
	@Test
	public void testCreerContactVide() {
		Assertions.assertThrows(ContactException.class, () -> this.contactService.creerContact(""));
	}
	
	@Test
	public void testCreerContactTropCourt() {
		Assertions.assertThrows(ContactException.class, () -> this.contactService.creerContact("ab"));
	}

	@Test
	public void testCreerContactTropLong() {
		Assertions.assertThrows(ContactException.class, () -> this.contactService.creerContact("Rhoshandiatelly-neshiaunneveshenk Koyaanfsquatsiuty"));
	}
	
	@Test
	public void testCreerContactOk() throws ContactException {
		Assertions.assertTrue(this.contactService.creerContact("TEST") != null);
	}
	
	@Test
	public void testCreerContactDoublon() throws ContactException {
		Contact contact = new Contact("Maël");
		Assertions.assertNotNull(contact);
		Assertions.assertEquals("Maël", contact.getName());
		contactService.creerContact("Maël");
	}
}
