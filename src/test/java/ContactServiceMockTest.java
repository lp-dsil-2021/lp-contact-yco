

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ContactServiceMockTest {

	@Mock
	private IContactDao contactDAO;
	
	@InjectMocks
	private ContactService contactService = new ContactService();
	
	@Captor
	ArgumentCaptor<Contact> argumentCaptor; 
	
	@Test
	public void testCreerContact() {
		Contact contact = new Contact("Thierry");
		Mockito.when(contactDAO.contains(contact)).thenReturn(false);
		
		Assertions.assertThrows(ContactException.class, () -> this.contactService.creerContact("Thierry"));
	}
	
	@Test
	public void testCreerContactFail() {
		Contact contact = new Contact("Thierry");
		Mockito.when(contactDAO.contains(contact)).thenReturn(true);
		
		Assertions.assertThrows(ContactException.class, () -> this.contactService.creerContact("Thierry"));
 	}
	
			
	@Test
	public void testCreerContactContient() {
		Mockito.when(contactDAO.contains(Mockito.any(Contact.class))).thenReturn(true);
		Assertions.assertThrows(ContactException.class, () -> this.contactService.creerContact("Thierry"));
	}
	
	@Test
	public void testCreerContactPass() throws ContactException {
		Mockito.when(contactDAO.contains(Mockito.any(Contact.class))).thenReturn(false);
		
		this.contactService.creerContact("Thierry");
		
		Mockito.verify(contactDAO).addContact(argumentCaptor.capture());
		Contact value = argumentCaptor.getValue();
		Assertions.assertEquals("Thierry", value.getName());
	}
	
	@Test
	public void testSearchContact() throws ContactException {	
		Mockito.when(contactDAO.searchContact("Thierry")).thenReturn(new Contact("Thierry"));
		Contact search = this.contactService.search("Thierry");
		Assertions.assertNotNull(search);
		Assertions.assertEquals("Thierry", search.getName());

	}
	
	@Test
	public void testSearchContactNotFound() throws ContactException {
		Mockito.when(contactDAO.searchContact("Thierry")).thenReturn(null);
		
		Assertions.assertThrows(ContactException.class, () -> this.contactService.search("Thierry"));

	}
	
	@Test
	public void testRemoveContact() throws ContactException {		
		Mockito.when(contactDAO.removeContact("Thierry")).thenReturn(true);
		Assertions.assertTrue(contactService.removeContact("Thierry"));
	}
	
	@Test
	public void testRemoveContactFail() throws ContactException {
		Mockito.when(contactDAO.removeContact("Thierry")).thenReturn(false);
		Assertions.assertFalse(contactService.removeContact("Thierry"));
	}
	
}
